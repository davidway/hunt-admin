package com.hunt.debugTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;

/**
 * @Author ouyangan
 * @Date 2017/1/17/13:40
 * @Description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring.xml"})
@Transactional
public class DebugTest {

    @Resource(name = "jmsQueueTemplate")
    private JmsTemplate jmsQueueTemplate;
    @Resource(name = "jmsTopicTemplate")
    private JmsTemplate jmsTopicTemplate;

    @Test
    public void testQueue() {
        for (int i = 0; i < 1000; i++) {
            sendMessage(String.valueOf(i + 1));
        }
        while (true) {

        }
    }

    @Test
    public void testTopic() {
        for (int i = 0; i < 1000; i++) {
            sendTopic(String.valueOf(i + 1));
        }
        while (true) {

        }
    }
    private void sendMessage(String msg) {
        jmsQueueTemplate.send(session -> session.createTextMessage(msg));
    }

    private void sendTopic(String msg) {
        jmsTopicTemplate.send(session -> session.createTextMessage(msg));
    }
}
