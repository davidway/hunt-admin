package com.hunt.service.impl;

import com.hunt.service.SmsService;
import com.hunt.util.OathUtil;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * @Author ouyangan
 * @Date 2017/1/16/15:28
 * @Description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring.xml"})
@Transactional
public class SmsServiceImplTest {
    private static final Logger log = LoggerFactory.getLogger(SmsServiceImplTest.class);
    @Autowired
    private SmsService smsService;

    @org.junit.Test
    public void send() throws Exception {
        HashMap<String, String> map= new HashMap<>();
        map.put("authCode", OathUtil.generateOathCode(4));
        smsService.send("18218933076", map);
    }

}